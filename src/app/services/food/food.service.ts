import { Injectable } from '@angular/core';
import { Food } from 'src/app/shared/models/Food';
import { Tag } from 'src/app/shared/models/Tag';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor() { }

  getFoodById(id: number): Food {
    return this.getAll().find(food => food.id == id)!;
  }

  getAllFoodsBySearchTerm(searchTerm: string): Food[] {
    return this.getAll().filter(food => 
      food.name.toLowerCase().includes(searchTerm.toLowerCase()));
  }

  getAllTags(): Tag[] {
    return [
      { name: 'ทั้งหมด', count: 14},
      { name: 'ฟาสต์ฟู้ด', count: 4},
      { name: 'พิซซ่า', count: 2},
      { name: 'อาหารกลางวัน', count: 3},
      { name: 'สโลว์ฟู้ด', count: 2},
      { name: 'แฮมเบอร์เกอร์', count: 1},
      { name: 'ทอด', count: 1},
      { name: 'ซุป', count: 1}
    ];
  }
  
  getAllFoodsByTag(tag: string): Food[] {
    //statement?doJob1:doJob2
    return tag == "ทั้งหมด" ? 
    this.getAll() :
    this.getAll().filter(food => food.tags?.includes(tag));
  }

  getAll(): Food[] {
    return [
      {
        id:1,
        name: 'พิซซ่า เปปเปอร์โรนี',
        cookTime: '10-20',
        price: 10,
        favorite: false,
        origins: ['อิตาลี'],
        stars: 4.5,
        imageUrl: '/assets/images/foods/food-1.jpg',
        tags: ['ฟาสต์ฟู้ด', 'พิซซ่า', 'อาหารกลางวัน'],
      },
      {
        id: 2,
        name: 'มีทบอล',
        price: 20,
        cookTime: '20-30',
        favorite: true,
        origins: ['เปอร์เซีย', 'ตะวันออกกลาง', 'จีน'],
        stars: 4.7,
        imageUrl: '/assets/images/foods/food-2.jpg',
        tags: ['สโลว์ฟู้ด', 'อาหารกลางวัน'],
      },
      {
        id: 3,
        name: 'แฮมเบอร์เกอร์',
        price: 5,
        cookTime: '10-15',
        favorite: false,
        origins: ['เยอรมนี', 'สหรัฐอเมริกา'],
        stars: 3.5,
        imageUrl: '/assets/images/foods/food-3.jpg',
        tags: ['ฟาสต์ฟู้ด', 'แฮมเบอร์เกอร์'],
      },
      {
        id: 4,
        name: 'มันฝรั่งทอด',
        price: 2,
        cookTime: '15-20',
        favorite: true,
        origins: ['เบลเยียม', 'ฝรั่งเศส'],
        stars: 3.3,
        imageUrl: '/assets/images/foods/food-4.jpg',
        tags: ['ฟาสต์ฟู้ด', 'ทอด'],
      },
      {
        id: 5,
        name: 'ซุปไก่',
        price: 11,
        cookTime: '40-50',
        favorite: false,
        origins: ['อินเดีย', 'เอเชีย'],
        stars: 3.0,
        imageUrl: '/assets/images/foods/food-5.jpg',
        tags: ['สโลว์ฟู้ด', 'ซุป'],
      },
      {
        id: 6,
        name: 'พิซซ่า ผักรวม',
        price: 9,
        cookTime: '40-50',
        favorite: false,
        origins: ['อิตาลี'],
        stars: 4.0,
        imageUrl: '/assets/images/foods/food-6.jpg',
        tags: ['ฟาสต์ฟู้ด', 'พิซซ่า', 'อาหารกลางวัน'],
      }
    ];
  }
}
